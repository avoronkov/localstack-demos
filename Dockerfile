FROM golang:1.13 as builder

WORKDIR /go/src/aws-demos
COPY go.mod go.sum ./
RUN go mod download

ENV CGO_ENABLED=0

COPY . .
RUN go build ./cmd/s3demo

FROM scratch

WORKDIR /app
COPY --from=builder /go/src/aws-demos/s3demo /app/
COPY --from=builder /go/src/aws-demos/examples/* /app/examples/

CMD ["./s3demo"]
