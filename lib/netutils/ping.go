package netutils

import (
	"log"
	"net"
	"net/url"
	"time"
)

// wait for endpoint to start up
func PingEndpoint(endpoint string) error {
	tries := 7
	delay := 5 * time.Second

	endpointUrl, err := url.Parse(endpoint)
	if err != nil {
		return err
	}
	host := endpointUrl.Host
	log.Printf("[INFO] Wait for %v to start...", host)
	for i := tries; i > 0; i-- {
		conn, err := net.Dial("tcp", host)
		if err == nil {
			_ = conn.Close()
			log.Printf("[INFO] OK.")
			return nil
		}
		time.Sleep(delay)
	}
	log.Printf("[INFO] Failed.")
	return nil
}
