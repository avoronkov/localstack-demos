package awsconf

import (
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
)

// The following environment variables are supported:
// AWS_ENDPOINT - aws endpoint
// AWS_REGION   - aws region (implicit)
// AWS_ACCOUNT_ID
// AWS_ACCESS_KEY - (TODO fix if this one is correct)
// VERBOSE      - verbose output

func New() *aws.Config {
	endpoint := os.Getenv("AWS_ENDPOINT")
	accessKeyID := os.Getenv("AWS_ACCOUNT_ID")
	secretAccessKey := os.Getenv("AWS_ACCESS_KEY")
	verbose := os.Getenv("VERBOSE") != ""

	return &aws.Config{
		Credentials:                   credentials.NewStaticCredentials(accessKeyID, secretAccessKey, ""),
		Endpoint:                      aws.String(endpoint),
		S3ForcePathStyle:              aws.Bool(true),
		CredentialsChainVerboseErrors: aws.Bool(verbose),
	}
}
