module aws-demos

go 1.13

require (
	github.com/aws/aws-sdk-go v1.25.48
	github.com/joho/godotenv v1.3.0
)
