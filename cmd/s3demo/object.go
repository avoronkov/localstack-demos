package main

import (
	"log"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func PutFile(client *s3.S3, bucket, filename string, acl string) error {
	if acl == "" {
		acl = "public-read"
	}

	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	input := &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filepath.Base(filename)),
		ACL:    aws.String(acl),
		Body:   f,
	}

	output, err := client.PutObject(input)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG] PutObject Output: %v", output)
	return nil
}
