package main

import (
	"log"

	"github.com/aws/aws-sdk-go/service/s3"
)

func CreateBucket(client *s3.S3, name string) error {
	input := &s3.CreateBucketInput{
		Bucket: &name,
	}
	output, err := client.CreateBucket(input)
	if err != nil {
		return err
	}
	log.Printf("[DEBUG] CreateBucket Output: %v", output)
	return nil
}
