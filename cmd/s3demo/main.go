package main

import (
	"aws-demos/lib/awsconf"
	"aws-demos/lib/netutils"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func doMain() int {
	InitEnv()

	config := awsconf.New()

	if config.Endpoint != nil {
		if err := netutils.PingEndpoint(*config.Endpoint); err != nil {
			log.Printf("[ERROR] pingEndpoint %v failed: %v", *config.Endpoint, err)
			return 1
		}
	}

	sess, err := session.NewSession(config)
	if err != nil {
		log.Printf("[ERR] NewSession failed: %v", err)
		return 1
	}
	s3Client := s3.New(sess, &aws.Config{})

	// Create bucket
	bucketName := "demobucket"
	if err := CreateBucket(s3Client, bucketName); err != nil {
		log.Printf("[ERR] CreateBucket failed: %v", err)
		return 1
	}

	// Upload file
	fileName := "examples/original.jpg"
	if err := PutFile(s3Client, bucketName, fileName, "public-read"); err != nil {
		log.Printf("[ERR] PutFile failed: %v", err)
		return 1
	}

	if config.Endpoint != nil {
		fmt.Printf("File is available at %v/%v/%v\n", *config.Endpoint, bucketName, filepath.Base(fileName))
	}

	return 0
}

func main() {
	os.Exit(doMain())
}
