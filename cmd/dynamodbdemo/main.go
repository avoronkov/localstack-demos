package main

import (
	"aws-demos/lib/awsconf"
	"aws-demos/lib/netutils"
	"fmt"

	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func doMain() int {
	InitEnv()

	config := awsconf.New()

	if config.Endpoint != nil {
		if err := netutils.PingEndpoint(*config.Endpoint); err != nil {
			log.Printf("[ERROR] pingEndpoint %v failed: %v", *config.Endpoint, err)
			return 1
		}
	}

	sess, err := session.NewSession(config)
	if err != nil {
		log.Printf("[ERR] NewSession failed: %v", err)
		return 1
	}

	client := dynamodb.New(sess)

	if err := CreateTable(client); err != nil {
		log.Printf("[ERR] CreateTable failed: %v", err)
		return 1
	}

	id := "d36f8f9425c4a8000ad9c4a97185aca5"

	item := &Item{
		Installation: id,
		Token:        "a12b7cb43c9d9134b5bb1b35e9096b66775d9e92e7611d1cc92b02edd6782a87",
		Status:       "In progress",
	}

	if err := PutItem(client, item); err != nil {
		log.Printf("[ERR] PutItem failed: %v", err)
		return 1
	}

	foundItem, err := FetchItem(client, id)
	if err != nil {
		log.Printf("[ERR] FetchItem(..., %v) failed: %v", id, err)
		return 1
	}
	if foundItem == nil {
		log.Printf("[ERR] FetchItem(..., %v) found nothing", id)
	} else {
		fmt.Printf("Item found: %v\n", foundItem)
	}

	nonexist, err := FetchItem(client, "1234567890abcdef1234567890abcdef")
	if err != nil {
		log.Printf("[ERR] FetchItem(..., %v) failed: %v", id, err)
		return 1
	}
	if nonexist != nil {
		log.Printf("[ERR] FetchItem(... ) accidentally found something: %v", nonexist)
	}
	return 0
}

func main() {
	os.Exit(doMain())
}
