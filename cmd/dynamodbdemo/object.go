package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var tableName = "Installations"

func CreateTable(client *dynamodb.DynamoDB) error {
	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Installation"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Installation"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		TableName: aws.String(tableName),
	}
	_, err := client.CreateTable(input)
	return err
}

type Item struct {
	// Id
	Installation string
	Token        string
	Status       string
}

func (i *Item) String() string {
	return fmt.Sprintf("Id: %v, Token: %v, Status: %v", i.Installation, i.Token, i.Status)
}

func PutItem(client *dynamodb.DynamoDB, item *Item) error {
	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return err
	}
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}
	_, err = client.PutItem(input)
	return err
}

func FetchItem(client *dynamodb.DynamoDB, id string) (*Item, error) {
	result, err := client.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Installation": {
				S: aws.String(id),
			},
		},
	})
	if err != nil {
		return nil, err
	}
	item := new(Item)
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		return nil, err
	}
	if item.Installation == "" {
		// item not found
		return nil, nil
	}
	return item, nil
}
