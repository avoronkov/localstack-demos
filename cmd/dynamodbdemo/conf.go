package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var localstackDynamodbEndpoint = "http://localhost:4569"

func InitEnv() {
	err := godotenv.Load("env.cfg")
	if err != nil {
		log.Printf("[WARN] loading env.cfg failed: %v", err)
	}
	if endpoint := os.Getenv("AWS_ENDPOINT"); endpoint == "" {
		if err := os.Setenv("AWS_ENDPOINT", localstackDynamodbEndpoint); err != nil {
			log.Fatal(err)
		}
	}
}
