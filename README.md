# AWS demos

## Run demos manually

1. Start localstack.
```
$ localstack start
```

(Check [github.com/localstack/localstack](https://github.com/localstack/localstack) for more details)

2. Run s3demo.
```
$ go run ./cmd/s3demo/
2019/12/08 18:34:56 [INFO] Wait for localhost:4572 to start...
2019/12/08 18:34:56 [INFO] OK.
2019/12/08 18:34:56 [DEBUG] CreateBucket Output: {

}
2019/12/08 18:34:56 [DEBUG] PutObject Output: {
  ETag: "\"3625144254bc93f02fae5a000d80bd8b\""
}
File is available at http://localhost:4572/demobucket/original.jpg
```

## Run demos with docker-compose

1. Start up (interrupt with `^C`).
```
$ docker-compose up
```

2. Cleanup.
```
$ docker-compose down
```
